#!/usr/bin/env python
"""
Dynamic DNS updater client for DNSExit.

dnsexitupdate.py is a dynamic DNS update client for DNSExit. The account user
name, password and host name are specified by command line parameters.  The
client uses DNSExit's web update API, which is specified in the document:
http://downloads.dnsexit.com/ipUpdateDev.doc.  

Usage:     python dnsexitupdate.py [options]

Options:
    -u USERNAME         DNSExit login user name

    -p PASSWORD         DNSExit IP update password

    -s HOST             Server hostname
                          e.g. yourhost.yourdomain.com

    -t TIME             Time interval between updates, in minutes.
                          If not specified, the default value is 15 minutes.

    -h, --help          Print this help file

Example:
  python dnsexitupdate.py -u fred -p secret -h homeserver.fred.com

This script is written for Python 2.7.x, which you can download from:
  http://www.python.org/download/

The script also runs OK under Python 3.4.x

"""

import sys, optparse, time, re
from time import sleep

try:
    # Python 3
    from urllib.request import urlopen, Request
    from urllib.parse import urlencode
except ImportError:
    # Python 2
    from urllib import urlencode
    from urllib2 import urlopen, Request

# get external IP address
# For details about icanhaz.com see http://major.io/icanhazip-com-faq/
def getExternalIP():
    ip_address = None
    try:
        ip = urlopen("http://ipv4.icanhazip.com/").read()
        # convert to unicode string and remove trailing whitespace and new line
        ip_address = ip.decode(encoding="utf-8").rstrip()
    except Exception as e:
        print("Failed to read external IP address.")
    return ip_address



# Update IP address by making a HTTP POST request to the DNSExit update service
def update(username, password, host, ipaddress):
    response = None
    try:
        # read the updater URL by making a GET request to DNSExit API
        response_body = urlopen("http://www.dnsexit.com/ipupdate/dyndata.txt").read().decode(encoding="utf-8")
        url = re.findall("url=\S+", response_body)[0][4:]
        # update the IP address by making a GET request to DNSExit RemoteUpdate service
        # making a POST request no longer works for RemoteUpdate version 2.0
        accountinfo = dict(login=username, password=password, host=host, myip=ipaddress)
        url = url + '?' + urlencode(accountinfo)
        req = Request(url)
        response = urlopen(req)
    except Exception as e:
        print("Failed to update IP address using DNSexit web API.")
    return response

########## MAIN SCRIPT ###########

ipaddress = "uninitialised"

# parse command line arguments
p = optparse.OptionParser(add_help_option=False)
p.add_option("-u", action="store", dest="username", help="DNSExit login user name.")
p.add_option("-p", action="store", dest="password", help="DNSExit IP update password.")
p.add_option("-s", action="store", dest="host", help="Server host name (e.g. yourhost.yourdomain.com)")
p.add_option("-t", action="store", dest="time", default=15, help="Time between updates, in minutes")
p.add_option("-h", "--help", action="store_true", dest="help", help="Print this help file.")
opts, args = p.parse_args()

# print help and exit
if opts.help:
    print(__doc__)
    sys.exit()

# enforce DNSExit's 8 minute minimum update period
update_interval = float(opts.time)
if update_interval < 8:
    update_interval = 8

while True:
    newipaddress = getExternalIP()
    if newipaddress is not None and newipaddress != ipaddress:
        # only send request to DNSExit's update API if IP address has changed
        ipaddress = newipaddress
        print("IP Address updated: " + time.ctime())
        print("Host: " + opts.host)
        print("IP address: " + ipaddress)
        response = update(opts.username, opts.password, opts.host, ipaddress)
        if response is not None and response.msg == "OK":
            # print result message, which is of the form 'n=message_text',
            # where n is a number
            response_body = response.read().decode(encoding="utf-8")
            try:
                result = re.split("=", re.findall("[\r\n]+[0-9]+=.*", response_body)[0])[1]
            except Exception as e:
                result = "Unexpected HTTP response body: '" + response_body + "'"
            print(result)
        else:
            print("No response from DNSExit's update web service")
    sys.stdout.flush()
    # sleep until next update (default 15 minutes)
    sleep(update_interval * 60)
