# dnsexitupdate #

The Python script ***dnsexitupdate.py*** is a dynamic DNS client for [DNSexit](http://www.dnsexit.com/).  DNSexit provides a full range of dynamic DNS services including free Second Level Domains (SLD), which provide a constant host name that points to the dynamic IP address allocated by your broadband ISP.  The DNS client posts an update to the DNSExit update web service whenever the host machine's dynamic IP address changes.  The client uses DNSexit's web API, which is specified in the documents:  

- [http://downloads.dnsexit.com/ipUpdate.doc](http://downloads.dnsexit.com/ipUpdate.doc "How to update host IP address on DNSexit").  
- [http://downloads.dnsexit.com/ipUpdateDev.doc](http://downloads.dnsexit.com/ipUpdateDev.doc "DNSexit API Reference").  


The dynamic DNS client is a Python script, so your machine must have either Python 2.7.x or Python 3.4.x installed in order to run it.   You can download Python source code and installers for most popular operating systems from:  [https://www.python.org/](https://www.python.org/)



## Using the dynamic DNS update client ##

To run the dynamic DNS update client, just download the file [dnsexitupdate.py](https://bitbucket.org/tbayley/dnsexitupdate/downloads/dnsexitupdate.py) from this project's downloads page: [https://bitbucket.org/tbayley/dnsexitupdate/downloads](https://bitbucket.org/tbayley/dnsexitupdate/downloads) and save it to a convenient location on your host machine, such as your home directory. No other files from this repository are required.

The script is run like any other Python script.  Basic usage is described below.  The account username, password and host name are specified by command line parameters when invoking the script.  

```
Usage:     python dnsexitupdate.py [options]

Options:
    -u USERNAME         DNSExit login user name

    -p PASSWORD         DNSExit IP update password

    -s HOST             Server hostname
                          e.g. yourhost.yourdomain.com

    -t TIME             Time interval between updates, in minutes.
                          If not specified, the default value is 15 minutes.

    -h, --help          Print this help file

Example:
  python dnsexitupdate.py -u bob -p passw0rd -s bob.publicvm.com
```


## Running the dynamic DNS update client as a Linux daemon ##

Normally you will want to run the dynamic DNS update client as a daemon, which runs continuously in the background and automatically re-starts when the machine re-boots or if an error occurs.  There are several ways to do this on a Linux host:


### Running the client manually ###

Copy the client script *dnsexitupdate.py* to your home directory.  The simplest way to run the client is to start it manually, whenever you reboot the machine.  If your username is *bob*, your password is *passw0rd* and your hostname is *bob.publicvm.com*, then the Linux bash command to start the client is:

```bash
python dnsexitupdate.py -u bob -p passw0rd -s bob.publicvm.com >> dnsexit.log 2>&1 &
```

Let's explain what this command does:

- ***>> dnsexit.log*** redirects the client's output *stdout* and appends it to the log file *dnsexit.log* in your home directory.  If you do not want to log the client's output, redirect *stdout* to */dev/null*, which is a dummy device that does not record any output. 
- ***2>&1*** redirects *stderr* to *stdout*, which in turn is redirected to the log file, so that both normal program output and run-time errors are recorded. 
- ***&*** at the end runs this command as a background task.

This command returns a four digit number, which is the process ID (PID).  You need to know the PID to stop the DNSexit client running.  However if you did not write it down when starting the client, you can find it by executing this command:

```bash
pgrep -f dnsexitupdate.py
```

This command returns a four digit PID, for example *5032*.  To terminate the DNSexit client process, execute the following command, substituting *5032* with the process ID on your own machine:

```bash
kill -KILL 5032
```


### Running the client as a cron job ###

Probably the simplest way to run the DNSexit client automatically whenever the host machine is rebooted is to schedule it as a CRON job.  Each user has their own CRON table, which contains all scheduled tasks.  Enter the following command to edit your CRON table:

```bash
crontab -e
```

Scroll down to the bottom of the table and add the following line, which runs the script automatically at reboot:

```
@reboot python dnsexitupdate.py -u bob -p passw0rd -s bob.publicvm.com >> dnsexit.log 2>&1
```

Refer to the section titled **Running the client manually** to see what the various parts of this command do. The prefix ***@******reboot*** is a CRON keyword, which specifies that the command should execute at boot time.

When you have finished editing the CRON table, press [Ctrl] + O to save your changes and then [Ctrl] + X to exit.  Reboot the machine to start running the DNSexit update client.


### Running the client as a service with systemd ###

CRON is available on virtually all Linux distributions and is easy to use.  However if the DNSexit client script crashes, it will not automatically restart.  The solution is to run *dnsexitupdate.py* as a daemon that auto-starts at boot-up and re-starts on failure.  This section describes how to do so using [systemd](https://freedesktop.org/wiki/Software/systemd/), a service manager that is available on several Linux distributions, including [Ubuntu Linux 15.04](http://releases.ubuntu.com/15.04/) and later.

The following systemd script starts the DNSexit update client automatically when the machine is booted up.  Store it as */etc/systemd/system/dnsexitupdate.service*.

```
[Unit]
Description=Start and stop DNSexit update client

[Service]
WorkingDirectory=/home/tony/dnsexit
ExecStart=/usr/bin/python3 dnsexitupdate.py -u bob -p passw0rd -s bob.publicvm.com >> dnsexit.log 2>&1
Restart=on-failure

[Install]
# Start at boot-up
WantedBy=multi-user.target
```

Obviously, you need to edit the *[Service]* section of this configuration file to specify your own home directory and DNSexit account credentials.  Run the following command to check the script for syntax errors:

```bash
systemd-analyze verify dnsexitupdate.service
```

Type the following commands to check the update client’s status, to start it and to stop it respectively.  If necessary, type *q* to exit:

```bash
systemctl status dnsexitupdate
systemctl start dnsexitupdate
systemctl stop dnsexitupdate
```


### Running the client as a service with Upstart ###

The previous section described how to use systemd to run *dnsexitupdate.py* as a daemon at boot-up and automatically re-start it on failure.  Ubuntu Linux releases before [Ubuntu Linux 15.04](http://releases.ubuntu.com/15.04/) use a different service manager: [Upstart](http://upstart.ubuntu.com/).

The following upstart script starts the DNSexit update client automatically when the machine is booted up.  Store it as */etc/init/dnsexitupdate.conf*.

```
# /etc/init/dnsexitupdate.conf
description "Start and stop DNSexit update client"
version "1.0"
author "Tony Bayley"

# When to start and stop the service
start on (local-filesystems and net-device-up IFACE=eth0)
stop on shutdown

# If the process quits unexpectedly trigger a respawn
respawn

script
    cd /home/bob
    exec python dnsexitupdate.py -u bob -p passw0rd -s bob.publicvm.com >> dnsexit.log 2>&1
end script
```

Obviously, you need to edit the *script* section of this configuration file to specify your own home directory and DNSexit account credentials.

Type the following commands to check the update client’s status, to start it and to stop it respectively:

```bash
sudo status dnsexitupdate
sudo start dnsexitupdate
sudo stop dnsexitupdate
```


## Software Development Notes ##

If you want to modify or debug the DNSexit update client, either clone this repository using git, or download a [zip archive](https://bitbucket.org/tbayley/dnsexitupdate/get/87ea3b6a8cb3.zip) of the entire dnsexitupdate repository from the project's downloads page: [https://bitbucket.org/tbayley/dnsexitupdate/downloads](https://bitbucket.org/tbayley/dnsexitupdate/downloads).

The project was developed using Microsoft [Visual Studio Express 2013 for Windows Desktop](http://www.microsoft.com/en-gb/download/details.aspx?id=40787) and [Python Tools for Visual Studio](http://pytools.codeplex.com/).  The repository includes the Visual Studio Project and solution files, which you may find useful if you want to use the same toolchain.
